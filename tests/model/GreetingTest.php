<?php

namespace test\helloworld\model;

use helloworld\model\Greeting;

class GreetingTest extends \PHPUnit_Framework_TestCase {

  /**
   * @covers \helloworld\model\Greeting::__construct
   * @covers \helloworld\model\Greeting::getText
   * @covers \helloworld\model\Greeting::getLanguage
   */
  public function testGreeting() {

    $entity = new Greeting("Hello World!","en-us");

    $this->assertEquals("Hello World!", $entity->getText());
    $this->assertEquals("en-us", $entity->getLanguage());

  }

}

?>

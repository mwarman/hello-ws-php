<?php

namespace helloworld\model;

/**
 * The Greeting model object.
 *
 * @package    helloworld\model
 * @author     Matt Warman
 * @copyright  LeanStacks
 * @license    https://opensource.org/licenses/Apache-2.0  Apache 2.0 License
 * @link       https://bitbucket.org/mwarman/php-hello-ws
 */
 class Greeting {

  private $text;

  private $language;

  public function __construct($text, $language) {
    $this->text = $text;
    $this->language = $language;
  }

  public function getText() {
    return $this->text;
  }

  public function getLanguage() {
    return $this->language;
  }

}

?>

# Hello Web Services

## Acknowledgements

This is a [LEAN**STACKS**](http://www.leanstacks.com/) solution.

LEAN**STACKS** offers several technology instruction video series, publications, and starter projects. For more information go to [LeanStacks.com](http://www.leanstacks.com).

## Languages

THis project is authored in PHP.

## Installation

### Fork the Repository

Fork the [Hello Web Services](https://bitbucket.org/mwarman/hello-ws-php/overview) project on Bitbucket.  Clone the project to the host machine.

### Dependencies

The project requires the following dependencies to be installed on the host machine:

* PHP 5.6+
* Apache Ant 1.9+

### Running

The project utilizes [Apache Ant](http://ant.apache.org/) for build, package, and test workflow automation.

The following Ant targets are the most commonly used.

#### init

The `init` target cleans the artifacts from the previous build and prepares the project for the next build.

#### lint

The `lint` target analyzes the project source code for syntax errors.

#### unit-test

The `unit-test` target executes all unit test suites and produces the following reports:

* JUnit in XML
* Code Coverage in Clover XML
* Code Coverage in XML
* Code Coverage in HTML
* Crap4J in XML
* TestDox in HTML

Individual reports are produced in the `/build/reports` directory.

#### static-analysis

The `static-analysis` target executes all static code analysis tools and produces the following reports:

* Lines of Code Report in CSV and XML
* PHP Dependencies Report in XML with Supporting SVG Images
* PHP Mess Detector Report in HTML and PMD XML
* PHP CodeSniffer Report in Checkstyle XML
* PHP Copy Paste Detector Report in XML

Individual reports are produced in the `build/reports` directory.

#### docs

The `docs` target executes all documentation generation tools and produces the following reports:

* PHP Dox source code API documentation in HTML

Documentation is generated to the `build/reports` directory.

#### build

The `build` target is the default project target.  The `build` target assembles the project source code and performs all of the initialization, linting, testing, analysis, and documentation targets described previously in this document.

Software engineers **MUST** run the `build` target without any lint errors or unit test failures before submitting a pull request to the project repository.



```

$ ant build

OR

$ ant

```

The `build` target is utilized by Continuous Integration (CI) servers to both prepare a versioned application artifact for deployment (or distribution) and to produce all build reports for display or trend analysis.
